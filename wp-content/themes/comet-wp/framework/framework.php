<?php

/* Options Framework */
/*require_once COMET_FW_DIR . '/admin/options.php';*/

/* Helper functions */
require_once COMET_FW_DIR . '/inc/helpers.php';

/* Menu Walker */
require_once COMET_FW_DIR . '/inc/menu.php';

/* Widgets */
require_once COMET_FW_DIR . '/inc/widgets.php';

/* Metaboxes */
require_once COMET_FW_DIR . '/inc/metaboxes.php';

/* WooCommerce */
require_once COMET_FW_DIR . '/inc/wc.php';

/* Plugin installation */
require_once COMET_FW_DIR . '/lib/class-tgm-plugin-activation.php';
require_once COMET_FW_DIR . '/inc/required-plugins.php';
